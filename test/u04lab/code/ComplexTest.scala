package u04lab.code

import org.junit.jupiter.api.Assertions._
import org.junit.jupiter.api.Test

class ComplexTest {
  val arrayOop: Array[Complex] = Array(ComplexOop(10,20), ComplexOop(1,1), ComplexOop(7,0))
  val arrayFp: Array[Complex] = Array(ComplexFp(10,20), ComplexFp(1,1), ComplexFp(7,0))

  @Test
  def testAddComplexOop(): Unit = {
    val complexSum = arrayOop(0) + arrayOop(1) + arrayOop(2)

    assertEquals(18.0, complexSum.re)
    assertEquals(21.0, complexSum.im)
  }

  @Test
  def testMultComplexOop(): Unit = {
    val complexMult = arrayOop(0) * arrayOop(1)

    assertEquals(-10.0, complexMult.re)
    assertEquals(30.0, complexMult.im)
  }

  @Test
  def testAddComplexFp(): Unit = {
    val complexSum = arrayFp(0) + arrayFp(1) + arrayFp(2)

    assertEquals(18.0, complexSum.re)
    assertEquals(21.0, complexSum.im)
  }

  @Test
  def testMultComplexFp(): Unit = {
    val complexMult = arrayFp(0) * arrayFp(1)

    assertEquals(-10.0, complexMult.re)
    assertEquals(30.0, complexMult.im)
  }

  @Test
  def testEqualityToStringOop(): Unit = {
    val complexOop: Complex = ComplexOop(1,1)

    assertFalse(complexOop.equals(ComplexOop(1,1)))
    assertNotEquals("Complex(1,1)", complexOop.toString)
  }

  @Test
  def testEqualityToStringFp(): Unit = {
    val complexFp: Complex = ComplexFp(1,1)

    assertTrue(complexFp.equals(ComplexFp(1,1)))
    assertNotEquals("ComplexFp(1,1)", complexFp.toString)
  }
}