package u04lab.code

trait Complex {
  def re: Double
  def im: Double
  def +(c: Complex): Complex
  def *(c: Complex): Complex
}

object ComplexOop {
  def apply(re:Double, im:Double):Complex = new ComplexOopImpl(re, im)
}

object ComplexFp {
  def apply(re:Double, im:Double):Complex = ComplexFpImpl(re, im)
}

class ComplexOopImpl(override val re: Double,
                     override val im: Double) extends Complex {

  override def +(c: Complex): Complex = ComplexOop(this.re + c.re,this.im + c.im)

  override def *(c: Complex): Complex =  ComplexOop(this.re * c.re - this.im * c.im, this.re * c.im + this.im * c.re)
}

case class ComplexFpImpl(override val re: Double,
                         override val im: Double) extends Complex {
  def apply(x:Int): Int = x*x

  override def +(c: Complex): Complex = ComplexFp(this.re + c.re,this.im + c.im)

  override def *(c: Complex): Complex = ComplexFp(this.re * c.re - this.im * c.im, this.re * c.im + this.im * c.re)
}

/** Hints:
  * - implement Complex with a ComplexImpl class, similar to PersonImpl in slides OK
  * - check that equality and toString do not work IN TESTS
  * - use a case class ComplexImpl instead, creating objects without the 'new' keyword
  * - check equality and toString now
  */